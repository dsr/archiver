Run the following in the working directory after unzipping Package.zip which needs to be downloaded from the repo's downloads section:

jar -cfe output.jar Main *


Notes:

jython_gui.py and Archiver.py have to be copied to the unzipped Package directory after each change prior to running the above command.

