from javax.swing import JButton, JFrame, JPanel, JTextField, JTextArea, JScrollPane, JFileChooser
from java.awt import GridBagLayout, GridBagConstraints, Insets
from Archiver import make_archive, search

start_directory = "/Volumes/MacVolume/_Job Archive Files_"

class App(object):
    """docstring for App"""
        
    def main(self):
             
        """docstring for main"""
        self.frame = JFrame('Archiver',\
                    defaultCloseOperation = JFrame.EXIT_ON_CLOSE)
        self.frame.setResizable(False)
        c = GridBagConstraints()
    
        self.panel = JPanel(GridBagLayout())
    ### Search old button

        self.button_old = JButton('Search', \
                actionPerformed= self.search)
        c.fill = GridBagConstraints.HORIZONTAL
        c.insets = Insets(2, 2, 2, 2)
        c.weightx = 0.5
        c.gridx = 0
        c.gridy = 1
        self.panel.add(self.button_old, c)

    
    ### Create Archive
        self.button_create = JButton('Create Archive', \
                actionPerformed = self.change_text)
        c.fill = GridBagConstraints.HORIZONTAL
        c.insets = Insets(2, 2, 2, 2)
        c.weightx = 0.5
        c.gridx = 2
        c.gridy = 1
        self.panel.add(self.button_create, c)

    ### Search text field
        self.search_text = JTextField("")
        c.fill = GridBagConstraints.HORIZONTAL
        c.insets = Insets(10, 5, 5, 5)
        c.gridx = 0
        c.gridy = 0
        c.weightx = 0.5
        c.gridwidth = 3
        self.panel.add(self.search_text, c)

    ### Results area
        self.results_area = JTextArea(30, 40)
        self.results_area.setLineWrap(True)
        self.results_scroll_pane = JScrollPane(self.results_area)
        c.fill = GridBagConstraints.HORIZONTAL
        c.gridx = 0
        c.gridy = 2
        c.weightx = 0.5
        self.panel.add(self.results_scroll_pane, c)
    
        self.frame.add(self.panel)
        self.frame.pack()
        #panel.add(results_area)
    
        self.frame.setVisible(True)
    def change_text():
        pass
        
    def search(self, event):
        choose_file = JFileChooser(start_directory)
        choice = choose_file.showDialog(self.panel, "Choose a file")
        if choice == JFileChooser.APPROVE_OPTION:
            file = choose_file.getSelectedFile()
            term = self.search_text.getText()
            try:
                search_results = search(term, file.getPath())
            except IOError:
                search_results = search(term, file.getPath() + '.txt')
            if len(search_results) == 0:
                self.results_area.append("Not found")
            else:
                for result in search_results:
                    self.results_area.append(result + "\n")


app = App().main()
