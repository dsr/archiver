from tkinter import *
from tkinter import filedialog, messagebox
from tkinter import ttk
from Archiver import *
import threading



path1 = "/Volumes/MACVOLUME/_Job Archive Files_"
path2 = "/Volumes/MacVolume/_Job Archive Files_"

class Searcher(threading.Thread):
    def __init__(self, type):
        threading.Thread.__init__(self, name = 'searcher')
        self.type = type
    def search_all_files(self):
        results_count = 0
        try:
            search_results.delete(1.0, "end")
        except:
            pass
        if os.path.exists(path1):
            dialog = path1
        else:
            dialog = path2
        #print(dialog)
        if dialog is not "":
            for r, d, f in os.walk(dialog):
                for archive in f:
                    results = search(term.get(), os.path.join(r,archive))
                    #print(results)
                    if results:
                        results_count += 1
                        for line in results:
                            search_results.insert("end", line + "\n", 'results')
            if results_count == 0:
                search_results.insert('end', "Not found", 'results')
    def search_specific(self):
        dialog = filedialog.askopenfilename()
        if dialog is not "":
            results = search(term.get(), dialog)
            if results:
                try:
                    search_results.delete(1.0, "end")
                except:
                    pass
                for line in results:
                    search_results.insert("end", line + "\n", 'results')
            else:
                search_results.insert("end", "Not found", 'results')
   
    def run(self):
        progress_bar.start()
        if self.type == 'all':
            self.search_all_files()
        elif self.type == 'one':
            self.search_specific()
        progress_bar.stop()





def search_all():
    s = Searcher('all')
    s.start()
    
def search_one():
    s = Searcher('one')
    s.start()

def create_archive():
    dialog = filedialog.askdirectory()
    save_dialog = filedialog.asksaveasfilename()
    make_archive(dialog, save_dialog)
    messagebox.showinfo(message = 'Archive Created')

    
root = Tk()
root.title("Archiver")

term = StringVar()
mainframe = ttk.Frame(root, padding="12 3 12 12")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))

progress_bar = ttk.Progressbar(mainframe, orient = HORIZONTAL, length = 100,  \
        mode = 'indeterminate')

progress_bar.grid(column = 1, row = 3, columnspan = 4,\
        sticky = (E,W))

term_box = ttk.Entry(mainframe, textvariable = term).grid(column = 1, row =1, \
        sticky = (N,W,E,S),  columnspan = 4)

search_results = Text(mainframe, state='normal', width=80, height=36)
search_results.tag_add('results', 1.0, 'end')
search_results.tag_configure('results', font='Helvetica 16')
search_results.grid(column = 1, row = 4, sticky = (N,W,E,S), columnspan = 4)

search_all_button = ttk.Button(mainframe, text = 'Search All Archives', \
        command = search_all).grid( \
        column=1, row=2, sticky =(W,E))

search_specific = ttk.Button(mainframe, text = "Search Specific Archive", \
                    command = search_one).grid(column = 2, \
                    row = 2, sticky = (W, E))

make_archive_button = ttk.Button(mainframe, text = 'Make Archive', \
        command = create_archive).grid( \
        column = 3, row = 2, sticky = (W, E), columnspan = 2)

s = ttk.Scrollbar(mainframe, orient = VERTICAL, command = search_results.yview)
s.grid(column = 5, row = 4, sticky = (N,S))

search_results['yscrollcommand'] = s.set


root.columnconfigure(0, weight = 1)
root.rowconfigure(0, weight = 1)

mainframe.columnconfigure(1, weight=3)
mainframe.columnconfigure(2, weight=3)
mainframe.columnconfigure(3, weight=3)
mainframe.columnconfigure(4, weight=1)
mainframe.rowconfigure(1, weight=0)
mainframe.rowconfigure(3, weight = 2)


root.mainloop()
