import os
import re

def make_archive(archive_source, archive_path):
    f_out = open(archive_path, 'w')
    
    for r, d, f in os.walk(archive_source):
        disc = r.split("/")[2]
        dir_path = "/".join(r.split("/")[2:])
        #dir_path = "/".join(dir)
        f_out.write(dir_path + "\n")
        for item in f:
            f_out.write("------- File: " + item + "\n")
    f_out.close()

def search(term, file):
    
    pattern = "(\\b|_)%s(\\b|_)" % (term.lower())

    try:

        f = open(file)
        results = [line for line in f if \
                re.search(pattern, line.lower()) is not None]
        #print results
        return results
        f.close()
    except UnicodeDecodeError:
        #return "Can't decode."
        f = open(file, encoding = 'ISO-8859-1')
        results = [line for line in f if \
                re.search(pattern, line) is not None]
        return results
        f.close()

def search_test(param):
    print(param)
#make_archive("/Volumes/Test Disc", "/Users/danr/Desktop/test_archive.txt")
#print(search("13318", "/Users/danr/Desktop/test_archive.txt"))
#print(search("J6172", "/Users/danr/Desktop/Jobs 2010.txt"))

